import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import httpServ from "../Service/http.service";

let initialState = {
  danhSachNguoiDung: [],
};

export const getDanhSachNguoiDungAction = createAsyncThunk(
  "nguoiDung/getDanhSachNguoiDung",
  async () => {
    const response = await httpServ.layDanhSachNguoiDung();
    return response.data.content;
  }
);

const nguoiDungSlice = createSlice({
  name: "nguoiDung",

  initialState,
  reducers: {
    setDanhSachNguoiDung: (state, action) => {
      state.danhSachNguoiDung = action.payload;
    },
  },

  extraReducers: {
    [getDanhSachNguoiDungAction.fulfilled]: (state, action) => {
      state.danhSachNguoiDung = action.payload;
    },
  },
});

export const { setDanhSachNguoiDung } = nguoiDungSlice.actions;

export default nguoiDungSlice.reducer;
