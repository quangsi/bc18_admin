import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import SecureView from "./HOC/SecureView/SecureView";
import DangNhap from "./Page/DangNhap/DangNhap";
import TrangChu from "./Page/TrangChu/TrangChu";

export default function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<SecureView Component={<TrangChu />} />} />
          <Route path="/login" element={<DangNhap />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}
