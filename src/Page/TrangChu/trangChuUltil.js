import { Tag } from "antd";

export const columnTable = [
  {
    title: "Họ tên",
    dataIndex: "hoTen",
    key: "hoTen",
  },

  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Số điện  thoại",
    dataIndex: "soDt",
    key: "soDt",
  },
  {
    title: "Loại tài khoản",
    dataIndex: "maLoaiNguoiDung",
    key: "maLoaiNguoiDung",

    render: (maLoaiNguoiDung) => {
      return maLoaiNguoiDung === "QuanTri" ? (
        // <span>Quản Trị</span>
        <Tag color="red">Quản Trị</Tag>
      ) : (
        // <span>Khách Hàng</span>
        <Tag color="green">Khách Hàng</Tag>
      );
    },
  },
];

// render: (text) => <a>{text}</a>,
// email: "babylone@gmail.com"
// hoTen: "Hào Nguyễn"
// maLoaiNguoiDung: "QuanTri"
// matKhau: "123456789"
// soDt: "0987654321"
// taiKhoan: "haoadmin"
