import { Table } from "antd";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setDanhSachNguoiDung } from "../../redux/nguoiDungSlice";
import httpServ from "../../Service/http.service";
import { columnTable } from "./trangChuUltil";
import "./TrangChu.css";
export default function TrangChu() {
  let dispatch = useDispatch();

  let danhSachNguoiDung = useSelector((state) => {
    return state.nguoiDungSlice.danhSachNguoiDung;
  });

  // console.log("danhSachNguoiDung", danhSachNguoiDung);

  useEffect(() => {
    httpServ
      .layDanhSachNguoiDung()
      .then((res) => {
        console.log(res);

        dispatch(setDanhSachNguoiDung(res.data.content));
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div id="trangChu" className="container mx-auto py-5">
      <Table columns={columnTable} dataSource={danhSachNguoiDung} />
    </div>
  );
}
