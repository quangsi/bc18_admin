import React from "react";
import { Form, Input, Button, message } from "antd";
import axios from "axios";
import { DOMAIN, TOKEN_CYBERSOFT } from "../../configURL/constant";
import { useNavigate } from "react-router-dom";
import localStorageServ from "../../Service/locaStorage.service";
// import { localServ } from "../../services/LocalServ";
// import { useDispatch } from "react-redux";
// import { SET_USER_INFOR } from "../../redux/constant/userConstant";
// import localStorageServ from "../../serviceWorker/locaStorage.service";
export default function DangNhap() {
  let history = useNavigate();

  // let dispatch = useDispatch();

  const onFinish = (values) => {
    console.log("Success:", values);

    let data = { ...values, maNhom: "GP01" };
    axios({
      url: DOMAIN + "/api/QuanLyNguoiDung/DangNhap",
      method: "POST",
      data: data,

      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    })
      .then((res) => {
        console.log(res);
        message.success("Đăng nhập thành công");
        // dispatch({
        //   type: SET_USER_INFOR,
        //   payload: res.data.content,
        // });
        // localServ.setUserInfor(res.data.content);

        // localStorageServ.userInfor.set(res.data.content);
        localStorageServ.userInfor.set(res.data.content);
        setTimeout(() => {
          history("/");
        }, 3000);
      })
      .catch((err) => {
        message.error(err.message);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="bg-gray-300 h-full w-full flex items-center">
      <div className="container bg-white mx-auto rounded-lg shadow p-5 ">
        <Form
          name="basic"
          labelCol={{
            span: 4,
          }}
          wrapperCol={{
            span: 24,
          }}
          initialValues={{
            remember: true,
          }}
          layout="vertical"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="on"
        >
          <Form.Item
            label="Tài khoản"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Trường này không được để rỗng!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Mật khẩu"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Trường này không được để rỗng!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item>
            <Button
              className="mx-auto block bg-red-600 text-white rounded  cursor-pointer"
              htmlType="submit"
            >
              Đăng nhập
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
