import React from "react";
import UserNav from "./UserNav";

export default function NavHeader() {
  return (
    <div className=" h-20 w-full px-10 shadow flex justify-between items-center ">
      <span className="text-red-500 text-xl font-bold">Cyber Movies</span>

      <div className="  space-x-3">
        <span>Lịch chiếu</span>
        <span>Cụm rạp</span>
        <span>Tin tức</span>
        <span>Ứng dụng</span>
      </div>

      <UserNav />
    </div>
  );
}
